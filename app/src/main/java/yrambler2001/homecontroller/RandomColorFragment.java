package yrambler2001.homecontroller;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static yrambler2001.homecontroller.MainActivity.dictt;
import static yrambler2001.homecontroller.Util.hashMap;
import static yrambler2001.homecontroller.Util.prepareLay;
import static yrambler2001.homecontroller.Util.putHM;
import static yrambler2001.homecontroller.Util.send;

@SuppressLint("SetTextI18n")
public class RandomColorFragment extends GridFragment {
    static char numClass = 4;
    static int[] led_len;
    static String color="FFFFFF";
    {
        count = 7;
        title = "LED";
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grid_lay, container, false);
        String[] namesArray = getResources().getStringArray(R.array.led_names);
        int[] led_max = getResources().getIntArray(R.array.static_max);
        int[] led_min = getResources().getIntArray(R.array.static_min);
        int[] led_curr = getResources().getIntArray(R.array.static_curr);
        count=led_curr.length;
        View[][] views = prepareLay(count, view, inflater, container, namesArray, led_max, led_min, led_curr);
        table = (TableLayout) views[0][0];
        row = (TableRow[]) views[1];
        textView = (TextView[]) views[2];
        seekBar = (DiscreteSeekBar[]) views[3];
        editText = (EditText[]) views[4];

        view.findViewById(R.id.fabDownload).setOnClickListener(v -> send(getActivity(), hashMap("doit", "info"), numClass));
        view.findViewById(R.id.fabUpload).setOnClickListener(v ->
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < count; i++) {
                NumberFormat formatter = new DecimalFormat(new String(new char[led_len[i]]).replace('\0', '0'));
                String ss = formatter.format(seekBar[i].getProgress());
                s.append(ss);
            }
            send(getActivity(), putHM(hashMap("led", s.toString()),"ledfunc",seekBar[0].getProgress()+""), numClass);
        });
        View fo=view.findViewById(R.id.fabOther);
        fo.setVisibility(View.VISIBLE);
        fo.setOnClickListener(v ->  ColorPickerDialogBuilder
                .with(getActivity())
                .setTitle("Choose color")
                .initialColor(Color.parseColor("#FF"+color))
                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                .density(20)
                .noSliders()
                .setOnColorSelectedListener(selectedColor -> send(getActivity(), hashMap("ledcolor", Integer.toHexString(selectedColor).substring(2,8)), numClass))
                .setPositiveButton("ok", (dialog, selectedColor, allColors) -> {})
                .setNegativeButton("cancel", (dialog, which) -> {})
                .build()
                .show());
        update();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        send(view.getContext(), hashMap("ledfunc", String.valueOf((int)numClass)), numClass);
    }


    static void update() {
        if (!dictt.isEmpty()) {
            String resp = dictt.get("led");
            if (resp != null) {
                char curptr = 0;
                for (char i = 0; i < count; i++) {
                    try {
                        editText[i].setText(Integer.toString(Integer.parseInt(resp.substring(curptr, curptr + led_len[i]))));//001 -> 1
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    curptr += led_len[i];
                }
            }
            String tmp = dictt.get("ledfunc");
            if (tmp != null) {
                editText[0].setText(tmp);
            }
            tmp = dictt.get("ledcolor");
            if (tmp != null) {
                color=tmp;
            }
        }
    }


}
