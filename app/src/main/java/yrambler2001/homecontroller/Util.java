package yrambler2001.homecontroller;


import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressLint("SetTextI18n")
public class Util {

    public static View[][] prepareLay(int count, View view, LayoutInflater inflater, ViewGroup container, String[] namesArray, int[] led_max, int[] led_min, int[] led_curr) {
        TableLayout table = view.findViewById(R.id.table);
        TableRow row[] = new TableRow[count];
        TextView textView[] = new TextView[count];
        DiscreteSeekBar seekBar[] = new DiscreteSeekBar[count];
        EditText editText[] = new EditText[count];
        for (int i = 0; i < count; i++) {
            row[i] = (TableRow) inflater.inflate(R.layout.row_with_slider, container, false);
            row[i].setId(View.generateViewId());
            seekBar[i] = row[i].findViewById(R.id.seekBarRow);
            seekBar[i].setId(View.generateViewId());
            editText[i] = row[i].findViewById(R.id.editTextRow);
            editText[i].setId(View.generateViewId());
            textView[i] = row[i].findViewById(R.id.textViewRow);
            textView[i].setId(View.generateViewId());

            seekBar[i].setMin(led_min[i]);
            seekBar[i].setMax(led_max[i]);

            textView[i].setText(namesArray[i]);
            editText[i].setText(Integer.toString(led_curr[i]));
            seekBar[i].setProgress(led_curr[i]);

            seektext(seekBar[i], editText[i], led_min[i], led_max[i]);

            table.addView(row[i]);
        }
        return new View[][]{{table}, row, textView, seekBar, editText};
    }

    public static void seektext(DiscreteSeekBar seekBar, EditText editText, int min, int max) {
        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                int i;
                try {
                    i = Integer.parseInt(s.toString());
                } catch (NumberFormatException e) {
                    i = min;
                }
                if (i >= min && i <= max)
                    seekBar.setProgress(i);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        editText.setOnFocusChangeListener((v, hasFocus) -> parseinput(v, min, max));
        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                parseinput(v, min, max);
                return true;
            }
            return false;
        });
        seekBar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser)
                    editText.setText(Integer.toString(progress));
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
            }
        });


    }

    public static void parseinput(View v, int min, int max) {
        int i;
        EditText edit = (EditText) v;
        try {
            i = Integer.parseInt(edit.getText().toString());
        } catch (NumberFormatException e) {
            i = min;
            edit.setText(Integer.toString(min));
        }
        if (i > max)
            edit.setText(Integer.toString(max));
        if (i < min)
            edit.setText(Integer.toString(min));
    }

    public static void send(WeakReference<Context> wr, final Map<String, String> params, int whichclass) {
        if (wr.get() != null)
            send(wr.get(), params, whichclass);
    }

    public static void send(final Context context, final Map<String, String> params, int whichclass) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            String URL = "http://yrambler2001.theworkpc.com:7086";
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, response ->
            {
                Log.i("VOLLEY", response);
                dict(response);

                if (whichclass == StaticFragment.numClass)
                    StaticFragment.update();
                else if (whichclass == RainbowFragment.numClass)
                    RainbowFragment.update();
                else if(whichclass == GradientFragment.numClass)
                    GradientFragment.update();
                else if(whichclass == RandomColorFragment.numClass)
                    RandomColorFragment.update();

            }, error -> {
                Log.e("VOLLEY", error.toString());
                Toast.makeText(context, error.toString(),
                        Toast.LENGTH_SHORT).show();
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                protected Map<String, String> getParams() {
                    params.put("psw", "TheBestPass");
                    return params;
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        try {
                            responseString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                        } catch (UnsupportedEncodingException e) {
                            responseString = new String(response.data);
                        }
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(),
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public static void dict(String s) {
        MainActivity.dictt.clear();
        List<String> args = Stream.of(s.replaceAll("\\r\\n|\\r|\\n", " ") //remove new line chars
                .split(" "))
                .filter(x -> x.startsWith("NP_") && x.endsWith("_PN"))
                .collect(Collectors.toList());
        for (String x : args) {
            String[] a = x.split("=");
            MainActivity.dictt.put(
                    a[0].replace("NP_", "").replaceAll("[\\W]|_", ""), //delete all non-alphanumeric values
                    a[1].replace("_PN", "").replaceAll("[\\W]|_", ""));
        }
    }

    public static HashMap<String, String> hashMap(String k, String v) {
        HashMap<String, String> hm = new HashMap<>();
        hm.put(k, v);
        return hm;
    }

    public static HashMap<String, String> putHM(HashMap<String, String> hm, String k, String v) {
        hm.put(k, v);
        return hm;
    }
}
