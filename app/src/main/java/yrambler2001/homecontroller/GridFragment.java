package yrambler2001.homecontroller;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
@SuppressLint("SetTextI18n")
public class GridFragment extends Fragment {
    public static int count;
    public String title;
    public TableLayout table;
    public TableRow row[];
    public TextView textView[];
    public DiscreteSeekBar seekBar[];
    public static EditText editText[];



@Override
public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    getActivity().setTitle(title);
}
}
